package com.tms.model.productmanagement;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 *@author Manish Kumar
 *@category Products Persistance Class 
 *@Date 1st-March-2016
 */

@Entity
@Table(name="Products_Details")
public class Products implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id 
	@Column(name="prod_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long prodId ;

	@Column(name="prod_name", nullable=false, unique=true)
	private String prodName;

	@Column(name="prod_qty", nullable=false)
	private int prodQty;

	
	@Column(name="prod_price", nullable=false)
	private float productPrice;

	@Column(name="prod_discount")
	private int prodDiscount;

	@Column(name = "prod_desc", nullable=false, length = 1000)
	private String prodDesc;

	
	@Column(name = "prod_type", nullable=false)
	private String prodType;

	@Column(name = "Prod_category", nullable=false)
	private String prodCategory;

	@Column(name = "prod_brand", nullable =false)
	private String prodBrand;

	@Column(name = "Prod_seller_name", nullable=false)
	private String prodSellerName;

	@Column(name = "Prod_image", nullable=false)
	private byte[] prodImage;

	public Products() {
	}

	

	public Long getProdId() {
		return prodId;
	}



	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}



	public String getProdName() {
		return prodName;
	}



	public void setProdName(String prodName) {
		this.prodName = prodName;
	}



	public int getProdQty() {
		return prodQty;
	}



	public void setProdQty(int prodQty) {
		this.prodQty = prodQty;
	}



	public float getProductPrice() {
		return productPrice;
	}



	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}



	public int getProdDiscount() {
		return prodDiscount;
	}



	public void setProdDiscount(int prodDiscount) {
		this.prodDiscount = prodDiscount;
	}



	public String getProdDesc() {
		return prodDesc;
	}



	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}



	public String getProdType() {
		return prodType;
	}



	public void setProdType(String prodType) {
		this.prodType = prodType;
	}



	public String getProdCategory() {
		return prodCategory;
	}

	public void setProdCategory(String prodCategory) {
		this.prodCategory = prodCategory;
	}

	public String getProdBrand() {
		return prodBrand;
	}

	public void setProdBrand(String prodBrand) {
		this.prodBrand = prodBrand;
	}

	public String getProdSellerName() {
		return prodSellerName;
	}



	public void setProdSellerName(String prodSellerName) {
		this.prodSellerName = prodSellerName;
	}


	public byte[] getProdImage() {
		return prodImage;
	}



	public void setProdImage(byte[] prodImage) {
		this.prodImage = prodImage;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((prodId == null) ? 0 : prodId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Products))
			return false;
		Products other = (Products) obj;
		if (prodId == null) {
			if (other.prodId != null)
				return false;
		} else if (!prodId.equals(other.prodId))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "User [prodId=" + prodId + ", prodName=" + prodName + ", prodQty=" + prodQty
				+ ", productPrice=" + productPrice + ", prodDesc=" + prodDesc + ", prodType=" + prodType+""
				+ ", prodCategory=" + prodCategory + ", prodBrand=" + prodBrand + ", prodSellerName=" + prodSellerName+""
				+ ", prodImage=" + prodImage+"]";
	}





}
