package com.tms.model.usermanagement;

import java.io.Serializable;
import java.sql.Blob;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.*;

/**
 *@author Manish Kumar
 *@category User Persistance Classes 
 *@Date 1st-March-2016
 */

@Entity
@Table(name="customer_registration")
public class User implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id ;

	@Column(name="cust_username", unique=true, nullable=false)
	private String userusername;

	@Column(name="cust_email_id", nullable=false)
	private String useremail;

	@Column(name="cust_password", nullable=false)
	private String userpassword;

	@Column(name="cust_RePassword", nullable=false)
	private String userrepassword;	
	
	@Column(name ="cust_country", length=50, nullable=false)
	private String usercountry;
	
	@Column(name = "cust_address_1", length = 1000, nullable=false)
	private String useraddress1;
	
	@Column(name="cust_status", nullable=false)
	private String status=Status.ACTIVE.getStatus();
	
	@Column(name="Cust_fname")
	private String custFname;

	@Column(name="Cust_lname")
	private String custLname;

	@Column(name="cust_mob_number")
	private String custMobNumber;

	@Column(name = "cust_land_line")
	private String custLandLine;

	@Column(name = "cust_address_2", length = 500)
	private String custAddress2;

	@Column(name = "cust_city", length = 100)
	private String custCity;

	@Column(name = "cust_landmark", length = 500)
	private String custLandMark;

	@Column(name = "cust_state", length= 50)
	private String custState;

	@Column(name = "cust_pincode")
	private Long custPincode;

	@Column(name ="cust_pic")
	private Blob custPic;

	@Column(name ="cust_remember_me", length=10)
	private String usercheckflag;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "customer_registration_USER_PROFILE", 
	joinColumns = { @JoinColumn(name = "user_id") }, 
	inverseJoinColumns = { @JoinColumn(name = "USER_PROFILE_ID") })
	private Set<UserProfile> userProfiles = new HashSet<UserProfile>();

	/*@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<UserDocument> userDocuments = new HashSet<UserDocument>();*/

	

	public User() {
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(Set<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustFname() {
		return custFname;
	}

	public void setCustFname(String custFname) {
		this.custFname = custFname;
	}

	public String getCustLname() {
		return custLname;
	}

	public void setCustLname(String custLname) {
		this.custLname = custLname;
	}



	public String getCustMobNumber() {
		return custMobNumber;
	}

	public void setCustMobNumber(String custMobNumber) {
		this.custMobNumber = custMobNumber;
	}

	public String getCustLandLine() {
		return custLandLine;
	}

	public void setCustLandLine(String custLandLine) {
		this.custLandLine = custLandLine;
	}

	public String getCustAddress2() {
		return custAddress2;
	}

	public void setCustAddress2(String custAddress2) {
		this.custAddress2 = custAddress2;
	}

	public String getCustCity() {
		return custCity;
	}

	public void setCustCity(String custCity) {
		this.custCity = custCity;
	}

	public String getCustLandMark() {
		return custLandMark;
	}

	public void setCustLandMark(String custLandMark) {
		this.custLandMark = custLandMark;
	}

	public String getCustState() {
		return custState;
	}

	public void setCustState(String custState) {
		this.custState = custState;
	}

	public Long getCustPincode() {
		return custPincode;
	}

	public void setCustPincode(Long custPincode) {
		this.custPincode = custPincode;
	}

	public Blob getCustPic() {
		return custPic;
	}

	public void setCustPic(Blob custPic) {
		this.custPic = custPic;
	}



	public String getUseremail() {
		return useremail;
	}



	public void setUseremail(String useremail) {
		this.useremail = useremail;
	}



	public String getUseraddress1() {
		return useraddress1;
	}



	public void setUseraddress1(String useraddress1) {
		this.useraddress1 = useraddress1;
	}



	public String getUsercountry() {
		return usercountry;
	}



	public void setUsercountry(String usercountry) {
		this.usercountry = usercountry;
	}



	public String getUserusername() {
		return userusername;
	}



	public void setUserusername(String userusername) {
		this.userusername = userusername;
	}



	public String getUserpassword() {
		return userpassword;
	}



	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}



	public String getUserrepassword() {
		return userrepassword;
	}



	public void setUserrepassword(String userrepassword) {
		this.userrepassword = userrepassword;
	}



	public String getUsercheckflag() {
		return usercheckflag;
	}



	public void setUsercheckflag(String usercheckflag) {
		this.usercheckflag = usercheckflag;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + custFname + ", lastName=" + custLname
				+ ", email=" + useremail + ", custMobNumber=" + custMobNumber + ", custLandLine=" + custLandLine+""
				+ ", custAddress1=" + useraddress1 + ", custAddress2=" + custAddress2 + ", custCity=" + custCity+""
				+ ", custLandMark=" + custLandMark + ", custState=" + custState + ", custCountry=" + usercountry+""
				+ ", custPincode=" + custPincode + ", custUsername=" + userusername + ", custPassword="
				+ userpassword+", userRePassword="+ userrepassword+", custPic=" + custPic + ", usercheckflag=" + usercheckflag+"]";
	}





}
