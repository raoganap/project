package com.tms.model.usermanagement;

public enum UserProfileType {
	
	USER("USER"),
	BUSINESS("BUSINESS");
	
	String userProfileType;
	
	private UserProfileType(String userProfileType){
		this.userProfileType = userProfileType;
	}
	
	public String getUserProfileType(){
		return userProfileType;
	}
	
}
