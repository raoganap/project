package com.tms.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.imageio.ImageIO;

public class Common implements Serializable{

	/**
	 * @author Manish Kumar
	 * @category Common Class
	 */
	private static final long serialVersionUID = 1L;

	byte[] imageInByte;

	public static void convertFromByteToImage(byte[] byteImage) throws IOException{
		InputStream in = new ByteArrayInputStream(byteImage);
		BufferedImage bImageFromConvert = ImageIO.read(in);

		ImageIO.write(bImageFromConvert, "png", new File(
				"D:/Imp/imgs/write/tms1.jpg"));
	}

	public static byte[] convertFromImageToByte(String imagePath) throws IOException{
		File file = new File(imagePath);
		byte[] bFile = new byte[(int) file.length()];

		FileInputStream fileInputStream = new FileInputStream(file);
		fileInputStream.read(bFile);
		fileInputStream.close();
		return bFile;
	}

}
