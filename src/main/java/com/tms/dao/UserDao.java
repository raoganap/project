package com.tms.dao;

import java.util.List;

import com.tms.model.productmanagement.Products;
import com.tms.model.usermanagement.User;

public interface UserDao {

	User findById(int id);

	User getLogin(String emailId);

	public void createUserRegister(User user) throws Exception;

	public void updateUserRegister(User user);

	public List<User> getEmailId(String emailId);
	
	public void changePassword(User user);
	
	public List<User> getUserDetailsByUsernameAndEmailId(String username, String emailId);

}

