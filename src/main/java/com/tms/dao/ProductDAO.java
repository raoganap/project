package com.tms.dao;

import java.util.List;

import com.tms.model.productmanagement.Products;


/**
 *@author Manish Kumar
 *@category ProductDAO 
 *@Date 1st-March-2016
 */

public interface ProductDAO {
	
	public void createProdRegister(Products products) throws Exception;
	
	public List<Products> getProductByProdId(long prodId) throws Exception;
	
	public List<Products> searchProductsByProductName(String prodName) throws Exception;
	
	public List<Products> searchProducts() throws Exception;
	
	public List<Products> getProductByProdName(String productName);
	
	public void updateProduct(Products products);
	
}

