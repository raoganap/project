package com.tms.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tms.dao.ProductDAO;
import com.tms.model.productmanagement.Products;

/**
 *@author Manish Kumar
 *@category ProductDAOImpl 
 *@Date 1st-March-2016
 */

@Repository
@Transactional
public class ProductDAOImpl implements ProductDAO {


	@Autowired
	SessionFactory sessionFactory;

	
	public void createProdRegister(Products products) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(products);
		tx.commit();
		session.close();		
	}

	@SuppressWarnings("unchecked")
	public List<Products> getProductByProdName(String productName) {
		return (List<Products>) this.sessionFactory.getCurrentSession().createQuery("from Products where prod_name ='"+productName+"'").list();
	}
	
	public void updateProduct(Products products) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(products);
		tx.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Products> getProductByProdId(long prodId) {
		return (List<Products>) this.sessionFactory.getCurrentSession().createQuery("from Products where prod_id ='"+prodId+"'").list();
	}

	@SuppressWarnings("unchecked")
	public List<Products> searchProducts() throws Exception {
		return (List<Products>) this.sessionFactory.getCurrentSession().createQuery("from Products").list();
	}

	@SuppressWarnings("unchecked")
	public List<Products> searchProductsByProductName(String prodName)
			throws Exception {
		return (List<Products>) this.sessionFactory.getCurrentSession().createQuery("from Products where prod_name like '"+prodName+"%'").list();
	}




}
