package com.tms.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tms.common.AbstractDao;
import com.tms.dao.UserDao;
import com.tms.model.productmanagement.Products;
import com.tms.model.usermanagement.User;
import com.tms.service.impl.UserServiceImpl;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {
	public static final Logger logger = Logger.getLogger(UserServiceImpl.class);
	
	@Autowired
	SessionFactory sessionFactory;
	
	public User findById(int id) {
		return getByKey(id);
	}

	public User getLogin(String emailId) {
		logger.info("UserDaoImpl - getLogin - Start");
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("useremail", emailId));
		logger.info("UserDaoImpl - getLogin - End");
		return (User) crit.uniqueResult();
	}
	
	@Transactional
	public int insertRow(User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(user);
		tx.commit();
		Serializable id = session.getIdentifier(user);
		session.close();
		return (Integer)id;
	}
	
	@Transactional
	public void createUserRegister(User user) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(user);
		tx.commit();
		//Serializable id = session.getIdentifier(user);
		session.close();
		
	}

	public void updateUserRegister(User user) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(user);
		tx.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<User> getEmailId(String emailId) {
		return (List<User>) this.sessionFactory.getCurrentSession().createQuery("from User where cust_email_id ='"+emailId+"'").list();
		
	}

	public void changePassword(User user) {
		this.sessionFactory.getCurrentSession().update(user);
		
	}

	public List<User> getUserDetailsByUsernameAndEmailId(String username,
			String emailId) {
		return (List<User>) this.sessionFactory.getCurrentSession().createQuery("from User where cust_username ='"+username+"' and cust_email_id ='"+emailId+"'").list();
	}

	

	
}
