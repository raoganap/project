package com.tms.service;

import java.util.List;

import com.tms.model.usermanagement.User;

/**
 *@author Manish Kumar
 *@category UserService
 *@Date 1st-March-2016
 */

public interface UserService {
	
	public User getLogin(String emailId); 
	
	public void createUserRegister(User user) throws Exception;
	
	public void updateUserRegister(User user) throws Exception;
	
	public List<User> getEmailId(String emailId);
	
	public User sendMail(String emailId);
	
	public void changePassword(User user);
	
	public List<User> getUserDetailsByUsernameAndEmailId(String username, String emailId);
	
	
	
}