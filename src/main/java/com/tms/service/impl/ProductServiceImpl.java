package com.tms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tms.dao.ProductDAO;
import com.tms.model.productmanagement.Products;
import com.tms.service.ProductService;

/**
 *@author Manish Kumar
 *@category ProductServiceImpl
 *@Date 1st-March-2016
 */

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductDAO productDAO;

	@Transactional
	public List<Products> createProdRegister(Products products) throws Exception {
		productDAO.createProdRegister(products);
		return getProductByProdName(products.getProdName());		
	}

	public List<Products> getProductByProdName(String  productName) throws Exception {
		return productDAO.getProductByProdName(productName);
	}

	public void updateProduct(Products products) throws Exception {
		long prodId = products.getProdId();

		List<Products> productsList = getProductByProdId(prodId);
		for(Products prods : productsList){			
				prods.setProdQty(products.getProdQty());
				prods.setProductPrice(products.getProductPrice());
				prods.setProdDesc(products.getProdDesc());
				prods.setProdBrand(products.getProdBrand());			
				prods.setProdType(products.getProdType());
				prods.setProdSellerName(products.getProdSellerName());
				productDAO.updateProduct(prods);			
		}
	}

	public List<Products> getProductByProdId(long prodId) throws Exception{
		return productDAO.getProductByProdId(prodId);
	}

	public List<Products> searchProducts() throws Exception {
		return productDAO.searchProducts();
	}

	public List<Products> searchProductsByProductName(String prodName)
			throws Exception {
		return productDAO.searchProductsByProductName(prodName);
	}



}
