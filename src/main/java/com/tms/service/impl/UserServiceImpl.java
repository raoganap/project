package com.tms.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tms.common.MailingSystem;
import com.tms.dao.UserDao;
import com.tms.model.productmanagement.Products;
import com.tms.model.usermanagement.User;
import com.tms.service.UserService;
import com.tms.tests.usermanagement.UserRegdTest;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
	public static final Logger logger = Logger.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDao userDAO;

	public User findById(int id) {
		return userDAO.findById(id);
	}

	public User getLogin(String emailId) {
		logger.info("UserServiceImpl - getLogin");
		return userDAO.getLogin(emailId);		
	}
	
	@Transactional
	public void createUserRegister(User user) throws Exception {
		userDAO.createUserRegister(user);
	}

	@Transactional
	public void updateUserRegister(User user) throws Exception {
		
		String username = user.getUserusername();
		String emailId = user.getUseremail();

		List<User> usersList = getUserDetailsByUsernameAndEmailId(username, emailId);
		for(User users : usersList){
			if(users.getUserusername().trim().equals(user.getUserusername().trim()) && users.getUseremail().trim().equals(user.getUseremail().trim())){
				users.setUsercountry(user.getUsercountry());
				users.setUseraddress1(user.getUseraddress1());
				users.setCustFname(user.getCustFname());
				users.setCustLname(user.getCustLname());			
				users.setStatus(user.getStatus());
				users.setCustFname(user.getCustFname());
				users.setCustLname(user.getCustLname());
				users.setCustMobNumber(user.getCustMobNumber());
				users.setCustLandLine(user.getCustLandLine());
				users.setCustAddress2(user.getCustAddress2());
				users.setCustCity(user.getCustCity());
				users.setCustLandMark(user.getCustLandMark());
				users.setCustState(user.getCustState());
				users.setCustPincode(user.getCustPincode());
				users.setCustPic(user.getCustPic());
				
				userDAO.updateUserRegister(users);
			}
		}
		
		
		
		 
	}

	@Transactional
	public List<User> getEmailId(String emailId) {
		return userDAO.getEmailId(emailId);
	}

	public User sendMail(String emailId){
		List<User> user = getEmailId(emailId);
		
		String subject = "TMS E-Shopper(ForgotPassowrd): Your password is ...";
		String body = null;

		for(User usr : user){
			if(usr.getUseremail().trim().equals(emailId.trim())){
				body = "Hi "+usr.getUserusername()+" |\r\n\r\n \t Your registered Password is : "+usr.getUserpassword();			
				String [] toEmailIds = {usr.getUseremail()};

				MailingSystem.sendMail(usr.getUseremail(), usr.getUserpassword(), toEmailIds, subject, body);
			}
		}
		return null;

	}

	public void changePassword(User user) {
		List<User> users = getEmailId(user.getUseremail());
		if(users.size()!=0){
			for(User usr : users){
				if(user.getUserpassword().trim().equals(user.getUserrepassword().trim())){
					usr.setUserpassword(user.getUserpassword());
					usr.setUserrepassword(user.getUserrepassword());					
					userDAO.changePassword(usr);
					System.out.println("Password updated");
				}
			}
		}else{
			System.out.println("Sorry! Account is not exists. Please Signup now");
		}

	}

	public List<User> getUserDetailsByUsernameAndEmailId(String username,
			String emailId) {
		return userDAO.getUserDetailsByUsernameAndEmailId(username, emailId);
	}

}
