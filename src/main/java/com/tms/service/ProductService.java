package com.tms.service;

import java.util.List;
import com.tms.model.productmanagement.Products;


/**
 *@author Manish Kumar
 *@category ProductService 
 *@Date 1st-March-2016
 */

public interface ProductService {
	
	public List<Products> createProdRegister(Products products) throws Exception;
	
	public List<Products> getProductByProdId(long prodId)throws Exception;
	
	public List<Products> searchProducts() throws Exception;
	
	public List<Products> searchProductsByProductName(String prodName) throws Exception;
	
	public void updateProduct(Products user) throws Exception;	
	
}