<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>User Register | TMS-Shopper</title>

<link rel="stylesheet" href="<c:url value='/static/css/style1.css' />">

</head>
<body>
	<div class="container">
		<section class="register">
			<h1>
				<a href="<c:url value='/login' />">Register on TMS-Shopper</a>
			</h1>
			<form:form id="registerForm" modelAttribute="user" method="post" action="saveUserRegister">
				<div class="reg_section personal_info">
					<h3>Your Personal Information</h3>
					<form:input path="userusername" placeholder="Your Desired Username"/>
      				<form:input path="useremail" placeholder="Your E-mail Address"/>
				</div>
				<div class="reg_section password">
					<h3>Your Password</h3>
					<form:input path="userpassword" placeholder="Your Password"/>
     			 	<form:input path="userrepassword" placeholder="Confirm Password"/>
				</div>
				<div class="reg_section password">
					<h3>Your Country</h3>
					<form:select path="usercountry">
						<option value="India">India</option>
						<option value="USA">USA</option>
						<option value="UK">UK</option>
						<option value="Parish">Parish</option>
						<option value="Others..">Others..</option>
					</form:select>
					<form:textarea path="useraddress1" placeholder="Your Full Address..."/>
				</div>
				<p class="terms">
					<label> 
						<form:checkbox path="usercheckflag" value="agree"/> I accept <a href="#">TMS-Shopper</a>&nbsp;Terms '&amp;' Condition
					</label>
				</p>
				<p class="submit">
					<input type="submit" name="commit" value="Already Registered" onclick="<c:url value='/login' />"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" name="commit" value="Register">
				</p>
			</form:form>
		</section>
	</div>

</body>
</html>