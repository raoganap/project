<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Login | TMS-Shopper</title>
<link href="<c:url value='/static/css/style.css" rel="stylesheet' />"
	type="text/css" media="all" />
</head>
<body>
	<!---main--->
	<div class="main">
		<div class="main-section">
			<div class="login-section">
				<h2><a href="<c:url value='/' />">TMS-Shopper - Home</a></h2>
				<div class="login-middle">
					<p>Log in with your email and password</p>
					<form>
						<input type="text" placeholder="Username"> 
						<input type="password" placeholder="Password">
					</form>
				</div>
				<div class="login-bottom">
					<div class="login-right">
						<form>
							<input type="submit"
								value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
						</form>
					</div>
				</div>
				<div class="login-bottom">
					<div class="login-left">
						<a href="<c:url value='/forgotpassword' />">Forgot your password?&nbsp;</a>
					</div>
					<div class="login-left">
						<a href="<c:url value='/newuser' />">&nbsp;Sign Up Now!</a>
					</div>
				</div>
				<div class="login-top">
					<ul>
						<li><a class="face" href="#"><span class="face"> </span>Facebook</a>
							<a class="twit" href="#"><span class="twit"> </span>Twitter</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</body>
</html>