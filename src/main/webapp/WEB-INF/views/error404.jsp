<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Under maintenance  | TMS-Shopper</title>    
    <link href="<c:url value='/static/css/bootstrap.min.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/font-awesome.min.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/prettyPhoto.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/price-range.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/animate.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/main.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/responsive.css' />" rel="stylesheet"></link>
    <script src="<c:url value='/static/js/jquery.js' />"></script>
	<script src="<c:url value='/static/js/bootstrap.min.js' />"></script>
	<script src="<c:url value='/static/js/jquery.scrollUp.min.js' />"></script>
	<script src="<c:url value='/static/js/price-range.js' />"></script>
    <script src="<c:url value='/static/js/jquery.prettyPhoto.js' />"></script>
    <script src="<c:url value='/static/js/main.js' />"></script>
</head><!--/head-->

<body>
	<div class="container text-center">
		<div class="logo-404">
			<a href="<c:url value='/' />"><img src="<c:url value='/static/images/home/TMC-Shopper - Logo_png_small.png' />" alt="" /></a>
		</div>
		<div class="content-404">
			<img src="<c:url value='/static/images/404/404.png' />" class="img-responsive" alt="" />
			<h1><b>OPPS!</b> We Could not find this Page</h1>
			<p>Uh... So it looks like you brock something. The page you are looking for has up and Vanished.</p>
			<h2><a href="<c:url value='/' />">Bring me back Home</a></h2>
		</div>
	</div>

</body>
</html>