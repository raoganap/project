<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Login | TMS-Shopper</title>
<link href="<c:url value='/static/css/bootstrap.min.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/font-awesome.min.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/prettyPhoto.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/price-range.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/animate.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/main.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/responsive.css' />"
	rel="stylesheet"></link>
<script src="<c:url value='/static/js/jquery.js' />"></script>
<script src="<c:url value='/static/js/bootstrap.min.js' />"></script>
<script src="<c:url value='/static/js/jquery.scrollUp.min.js' />"></script>
<script src="<c:url value='/static/js/price-range.js' />"></script>
<script src="<c:url value='/static/js/jquery.prettyPhoto.js' />"></script>
<script src="<c:url value='/static/js/main.js' />"></script>
<link href="<c:url value='/static/css/style.css" rel="stylesheet' />"
	type="text/css" media="all" />
</head>
<body>
	<div class="main">
		<div class="main-section">
			<div class="login-section">
				<h2>
					<a href="<c:url value='/' />">Reset Password-TMS</a>
				</h2>
				<div class="login-middle">
					<form action="#" class="searchform">
						<input type="text" placeholder="Your email address" />
						<button type="submit" class="btn btn-default">
							<i class="fa fa-arrow-circle-o-right"></i>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>