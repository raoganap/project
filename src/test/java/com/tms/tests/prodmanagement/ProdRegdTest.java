package com.tms.tests.prodmanagement;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.tms.common.Common;
import com.tms.common.MailingSystem;
import com.tms.configuration.HibernateConfiguration;
import com.tms.model.productmanagement.Products;
import com.tms.service.ProductService;
import com.tms.tests.usermanagement.UserRegdTest;




@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=HibernateConfiguration.class,loader=AnnotationConfigWebContextLoader.class)
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class ProdRegdTest {

	public static final Logger logger = Logger.getLogger(ProdRegdTest.class);
	@Autowired
	ProductService productService; 

	@BeforeClass
	public static void setUp() {
		System.out.println("-----> SETUP <-----");
	}

	@Ignore
	@Test
	public final void createProdRegisterTest() throws Exception{
		Products products = new Products();
		products.setProdName("Shirt 1");
		products.setProdQty(12);
		products.setProductPrice(104);
		products.setProdDesc("Logo Desc");
		products.setProdBrand("Adidas");
		products.setProdCategory("Men ");
		products.setProdType("T-Shirt");
		products.setProdSellerName("TMS Seller");
		products.setProdImage(Common.convertFromImageToByte("D:\\Imp\\imgs\\14-Testcases.png"));

		List<Products> product = productService.createProdRegister(products);
		for (Products prods : product){
			if(prods.getProdName().trim().equals(products.getProdName())){
				logger.info("Your product Id is : "+prods.getProdId());
			}
		}
		Assert.assertTrue(true);
	}

	@Ignore
	@Test
	public final void updateProductsTest() throws Exception{
		Products products = new Products();
		products.setProdId(1L);

		products.setProdName("Logo12");
		products.setProdCategory("Women");		
		products.setProdQty(13);
		products.setProductPrice(14);
		products.setProdDesc("Logo Desc");
		products.setProdBrand("Adidas");
		products.setProdType("T-Shirt");
		products.setProdSellerName("TMS Seller");
		products.setProdImage(Common.convertFromImageToByte("D:\\Imp\\imgs\\14-Testcases.png"));

		productService.updateProduct(products);
		logger.info("Product detail is updated.");
		Assert.assertTrue(true);
	}

	@Ignore
	@Test
	public final void getProductsTest() throws Exception{

		List<Products> productsList = productService.getProductByProdId(1L);
		for(Products prods : productsList){			
			logger.info("Product id is : "+prods.getProdId());
			logger.info("Product Name is : "+prods.getProdName());
			logger.info("Product Category is : "+prods.getProdCategory());
			logger.info("Product Brand is : "+prods.getProdBrand());
			logger.info("Product Desc is : "+prods.getProdDesc());
			logger.info("Product Qty is : "+prods.getProdQty());
			logger.info("Product Price is : "+prods.getProductPrice());
			logger.info("Product Descount is : "+prods.getProdDiscount());
			logger.info("Product Type is : "+prods.getProdType());
			logger.info("Product Seller Name is : "+prods.getProdSellerName());
			Common.convertFromByteToImage(prods.getProdImage());
		}
		Assert.assertTrue(true);
	}

	//@Ignore
	@Test
	public final void searchProductsTest() throws Exception{

		List<Products> productsList = productService.searchProducts();
		for(Products prods : productsList){			
			logger.info("Product id is : "+prods.getProdId());
			logger.info("Product Name is : "+prods.getProdName());
			logger.info("Product Category is : "+prods.getProdCategory());
			logger.info("Product Brand is : "+prods.getProdBrand());
			logger.info("Product Desc is : "+prods.getProdDesc());
			logger.info("Product Qty is : "+prods.getProdQty());
			logger.info("Product Price is : "+prods.getProductPrice());
			logger.info("Product Descount is : "+prods.getProdDiscount());
			logger.info("Product Type is : "+prods.getProdType());
			logger.info("Product Seller Name is : "+prods.getProdSellerName());
			Common.convertFromByteToImage(prods.getProdImage());
		}
		Assert.assertTrue(true);
	}

	//@Ignore
	@Test
	public final void searchProductsByProductNameTest() throws Exception{
		
		String prodName = "shirt";
		
		List<Products> productsList = productService.searchProductsByProductName(prodName);
		for(Products prods : productsList){			
			logger.info("Product id is : "+prods.getProdId());
			logger.info("Product Name is : "+prods.getProdName());
			logger.info("Product Category is : "+prods.getProdCategory());
			logger.info("Product Brand is : "+prods.getProdBrand());
			logger.info("Product Desc is : "+prods.getProdDesc());
			logger.info("Product Qty is : "+prods.getProdQty());
			logger.info("Product Price is : "+prods.getProductPrice());
			logger.info("Product Descount is : "+prods.getProdDiscount());
			logger.info("Product Type is : "+prods.getProdType());
			logger.info("Product Seller Name is : "+prods.getProdSellerName());
			Common.convertFromByteToImage(prods.getProdImage());
		}
		Assert.assertTrue(true);
	}

		


}
