package com.tms.tests.usermanagement;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.tms.configuration.HibernateConfiguration;
import com.tms.model.productmanagement.Products;
import com.tms.model.usermanagement.User;
import com.tms.service.UserService;


/**
 *@author Manish Kumar
 *@category User-Business Registration test case
 *@Date 1st-March-2016
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=HibernateConfiguration.class,loader=AnnotationConfigWebContextLoader.class)
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class UserRegdTest {
	public static final Logger logger = Logger.getLogger(UserRegdTest.class);
	@Autowired
	UserService userService; 

	@BeforeClass
	public static void setUp() {
		System.out.println("-----> SETUP <-----");
	}

	@Ignore
	@Test
	public final void getLoginTest() throws Exception{
		logger.info("UserRegdTest - getLoginTest - Start");
		String emailId = "tmsmanishk@gmail.com";
		userService.getLogin(emailId);
		logger.info("UserRegdTest - getLoginTest - Completed");
		Assert.assertTrue(true);
	}

	@Ignore
	@Test
	public final void forgotAccountPasswordTest() throws Exception{
		logger.info("UserRegdTest - forgotAccountPasswordTest - Start");
		userService.sendMail("tmsmanishk@gmail.com");
		logger.info("UserRegdTest - forgotAccountPasswordTest - Completed");
		Assert.assertTrue(true);
	}

	@Ignore
	@Test
	public final void changeAccountPasswordTest() throws Exception{
		logger.info("UserRegdTest - changeAccountPasswordTest - Start");
		User user = new User();
		user.setUseremail("tmssatya@gmail.com");
		user.setUserpassword("satya@1234sa");
		user.setUserrepassword("satya@1234sa");

		userService.changePassword(user);
		logger.info("UserRegdTest - changeAccountPasswordTest - Completed");
		Assert.assertTrue(true);
	}

	@Ignore
	@Test
	public final void createUserRegisterTest() throws Exception{
		logger.info("UserRegdTest - createUserRegisterTest - Start");
		User user = new User();
		user.setUseremail("tmsmanishk@gmail.com");
		user.setUserusername("manishk");
		user.setUserpassword("manish@123");
		user.setUserrepassword("manish@123");
		user.setUseraddress1("Bangalore");
		user.setUsercountry("India");

		userService.createUserRegister(user);
		logger.info("UserRegdTest - createUserRegisterTest - Completed");
		Assert.assertTrue(true);
	}

	//@Ignore
	@Test
	public final void updateUsersTest() throws Exception{
		User user = new User();
		user.setUserusername("manishk");
		user.setUseremail("tmsmanishk@gmail.com");

		user.setCustCity("Delhi");

		userService.updateUserRegister(user);

		Assert.assertTrue(true);
	}



}
